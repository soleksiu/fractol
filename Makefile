#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/08 16:02:30 by soleksiu          #+#    #+#              #
#    Updated: 2018/07/08 16:02:35 by soleksiu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

SRCDIR = source_files

OBJDIR = object_files

SRC = main.c ft_error.c ft_flags_manager.c ft_init_fractal_params.c ft_check_fract_name.c \
	ft_draw_image.c ft_put_pixel_to_img.c \
	ft_fract_mandelbrot.c ft_fract_burn_ship.c ft_fract_julia.c \
	ft_fract_newton.c ft_fract_tricorn.c ft_fract_heart.c ft_fract_celtic.c\
	ft_fract_quasi.c ft_fract_star.c ft_fract_custom.c\
	ft_get_color.c ft_get_asid_color.c ft_find_fonts_color.c \
   	ft_keys_manager.c ft_check_key_code.c ft_print_controls_menu.c \
   	ft_display_help.c \
   	

OBJ = $(SRC:.c=.o)
OBJS = $(addprefix $(OBJDIR)/,$(SRC:.c=.o))

NAME = fractol

WRN =  -Wall -Wextra -Werror

PTEST = ../test_fdf/

all: objdir $(NAME)

$(NAME): objdir lib $(OBJS)
	@gcc $(WRN) $(OBJS) -L ./libft -lft -L ./lib_mlx -lmlx  -framework OpenGL -framework AppKit -o $(NAME) 

lib:
	@make -C ./libft/

objdir: 
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	@gcc $(WRN) -I includes -I /usr/local/include -c $< -o $@  

man:
	man /usr/share/man/man3/$(ENTRY).1

test: all
	@./$(NAME) ${fractol}

backup: fclean
	@cp -r * ~/Desktop/backup_fractol

ext_backup: fclean
	@@mkdir -p /Volumes/Untitled/projects_backups/backup_fractol
	@cp -r * /Volumes/Untitled/projects_backups/backup_fractol

fbackup: backup ext_backup

clean:
	@rm -rf $(OBJ)
	@make clean -C ./libft

fclean: clean
	@find . -name "*~" -delete -or -name "#*#" -delete -or -name ".DS_Store" -delete -or -name "a.out" -delete
	@make fclean -C ./libft/
	@rm -rf $(NAME)
	@rm -rf $(OBJDIR)

re: fclean all