# Fractol - Computer graphics project.
### Language of development: C
 This is a learning project of Unit Factory coding school, which using 
 'ecole 42' franchise and same learning programm as the network of innovative
 schools across the world in such countries as USA, France, Netherlands.
 
 This project is meant to create graphically beautiful fractals.
 

 To make life funnier, using the external libraries or frameworks was
 forbidden or very limited for student projects. Libft included in repository
 was developed by author as another lerning project in school 42. Available 
 external functions for this project is: open, read, write, close, malloc, free,
 perror, strerror, exit and functions from the math library.
 Also for graphic-specific projects we using MinilibX library which had ability 
 to initialise window, process keys events and output the image to the window.
 All additional stuff associated with images generating/processing, parsing 
 data, etc. student must done by yourself.
 
 NOTE: Project was developed and tested on macOS High Sierra 10.13.3.
 Unfortunately I have no ability to test it on other platforms for now.

## Installation
Use make to compile the project.

```make fractol```
## Usage
Programm is getting fractol name as input parameter.
To explore the beauty of fractal weaves user can move camera, and changing scale
using keys or mouse wheel. Zooming on point implemented for mouse interaction.

## The list of fractals which can be rendered using this project:
 * Mandelbrot
 * Julia
 * Tricorn
 * Burning ship
 * Heart
 * Celtic
 * Quasi
 * Star
 * Newton
 * Custom
Execute with: 
```./fractol fractol_name```

## Supported flags
![Screenshot](screenshots/help.png)
## Interface screenshots
![Screenshot](screenshots/burning_ship.png)
### Controlls menu
![Screenshot](screenshots/mandelbrot.png)
### Julia fractol
Programm tracking the mouse movement and changing fractol shape based on
cursor coords. Iteraction can be paused using 'P' key.
![Screenshot](screenshots/julia.png)
### Alternative color modes
![Screenshot](screenshots/custom.png)
![Screenshot](screenshots/star_other.png)
