/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:02:51 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:02:54 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <mlx.h>
# include <pthread.h>
# include "libft.h"
# include <stdio.h>

# define M_WIDTH   1280
# define M_HEIGHT  800

# define KEY_A     123
# define KEY_S     125
# define KEY_D     124
# define KEY_W     126
# define KEY_Z     6
# define KEY_X     7
# define KEY_Q     12
# define KEY_E     14
# define KEY_R     15
# define KEY_LAR   43
# define KEY_RAR   47
# define KEY_M     46
# define KEY_P     35
# define KEY_ESC   53
# define KEYS_NBR  14
# define FRCT_NBR  10
# define THREADS_NBR  42

typedef enum		e_names
{
	mandelbrot = 1,
	julia = 2,
	tricorn = 3,
	burning_ship = 4,
	heart = 5,
	celtic = 6,
	quasi = 7,
	star = 8,
	newton = 9,
	custom = 10
}					t_enames;

typedef struct		s_fractname
{
	t_enames		val;
	const char		*fract_name;

}					t_fractname;

typedef struct		s_coord
{
	double			x_b;
	double			y_b;
	double			x_e;
	double			y_e;
	int				i;
}					t_coord;

typedef struct		s_cmplx
{
	double			re;
	double			im;
}					t_cmplx;

enum				e_cmp
{
	alpha = 3,
	red = 2,
	green = 1,
	blue = 0

};

typedef union		u_color
{

	unsigned int	color;
	unsigned char	rgb[4];
}					t_color;

typedef struct		s_fractol
{
	char			*img_addr;
	int				iter_nb;
	int				default_iter;
	int				window_width;
	int				window_height;
	int				img_bpp;
	int				img_size_line;
	int				img_endian;
	unsigned int	color;
	double			zoom;
	double			move_x;
	double			move_y;
	double			c_re;
	double			c_im;
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	void			*img_rec_ptr;
	char			*img_rec_addr;
	int				fract_type;
	int				fract_type2;
	int				flag_c;
	int				flag_d;
	int				flag_h;
	int				flag_a;
	int				flag_i;
	int				flag_m;
	char			*mode;
	unsigned int	default_color;
	unsigned int	flag_color;
	int				menu;
	int				pause;
	int				*keys_arr;
	int				mult_window;
	int				mouse_motion;
	int				mouse_lk;
	int				mouse_x;
	int				mouse_y;
	int				mouse_dx;
	int				mouse_dy;
}					t_frct;

typedef	struct		s_thread
{
	char			*img_addr;
	int				iter_nb;
	int				window_width;
	int				window_height;
	int				img_bpp;
	int				img_size_line;
	int				img_endian;
	unsigned int	color;
	double			zoom;
	double			move_x;
	double			move_y;
	double			c_re;
	double			c_im;
	int				begin_x;
	int				begin_y;
	int				dx;
	int				dy;
	int				mouse_motion;
	char			*flag_m;
	unsigned int	flag_color;
}					t_thread;

void				*fractal_ft_ptr(void *data);
void				ft_init_fractal_params(t_frct *data);
void				*ft_fract_mandelbrot(void *data);
void				*ft_fract_julia(void *data);
void				*ft_fract_burn_ship(void *data);
void				*ft_fract_tricorn(void *data);
void				*ft_fract_heart(void *data);
void				*ft_fract_celtic(void *data);
void				*ft_fract_quasi(void *data);
void				*ft_fract_star(void *data);
void				*ft_fract_newton(void *data);
void				*ft_fract_custom(void *data);
unsigned int		ft_find_fonts_color(t_frct *data, char *type);
unsigned int		ft_get_color(int mi, int i, t_thread *data, t_cmplx z);
unsigned int		ft_get_asid_color(int mi, int i, unsigned int def_c);
void				ft_keys_manager(t_frct *data);
void				ft_print_controls_menu(t_frct *data);
void				ft_draw_image(t_frct *data);
void				ft_put_pixel_img(t_thread *d, int x, int y, unsigned int c);
void				ft_flags_manager(int argc, char **argv, t_frct *data);
void				ft_error(char *str, t_frct *data);
void				ft_display_help(t_frct *data);
int					ft_check_key_code(int key_nb, t_frct *data);
int					ft_check_fract_name(t_frct *data, char *argv);

#endif
