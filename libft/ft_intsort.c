/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 12:18:59 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/13 12:33:57 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	ft_intsort(int *ar, size_t arlen)
{
	size_t	i;
	size_t	j;
	int		buf;

	i = 0;
	buf = 0;
	j = arlen - 1;
	if (!ar || !arlen)
		return ;
	while (j)
	{
		while (i < j)
		{
			if (ar[i] > ar[i + 1])
			{
				buf = ar[i];
				ar[i] = ar[i + 1];
				ar[i + 1] = buf;
			}
			i++;
		}
		i = 0;
		j--;
	}
}
