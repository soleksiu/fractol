/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 16:54:46 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/09 21:08:24 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t i;
	size_t ds;
	size_t r;
	size_t s;

	s = 0;
	ds = ft_strlen(dst);
	if (dstsize < ds + 1)
		return (ft_strlen(src) + dstsize);
	r = ft_strlen(dst) + ft_strlen(src);
	if (dstsize > r)
		i = ft_strlen(src);
	else
		i = dstsize - ds - 1;
	while (i > 0 && src[s])
	{
		dst[ds++] = src[s++];
		i--;
	}
	dst[ds] = '\0';
	return (r);
}
