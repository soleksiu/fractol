/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:03:26 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/11 17:53:04 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

static size_t	ft_wrdcnt(char const *str, char c)
{
	char	*p;
	size_t	i;

	p = (char *)str;
	i = 0;
	while (*p)
	{
		while (*p == c)
			p++;
		if (*p)
		{
			i++;
			while (*p != c && *p)
				p++;
		}
	}
	return (i);
}

static char		*ft_wrdcut(const char *s, char c)
{
	char	*b;
	char	*e;
	char	*n;

	b = (char *)s;
	while (*b == c)
		b++;
	e = b;
	while (*(e + 1) != c && *e)
		e++;
	if (!(n = ft_strnew((e - b) + 2)))
		return (NULL);
	ft_strncpy(n, b, (e - b) + 1);
	return (n);
}

static void		ft_arfree(char **str, size_t nb)
{
	while (nb--)
		free(str[nb]);
	free(str);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**p;
	size_t	wrdnb;
	size_t	i;

	i = 0;
	if (!s)
		return (NULL);
	wrdnb = ft_wrdcnt(s, c);
	if (!(p = (char **)malloc(sizeof(p) * (wrdnb + 1))))
		return (NULL);
	while (i < wrdnb)
	{
		while (*s == c && *s)
			s++;
		p[i++] = ft_wrdcut(s, c);
		if (!p[i - 1])
		{
			ft_arfree(p, wrdnb);
			return (NULL);
		}
		while (*s != c && *s)
			s++;
	}
	p[i] = NULL;
	return (p);
}
