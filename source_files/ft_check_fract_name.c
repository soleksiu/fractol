/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_fract_name.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 17:38:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/10 17:38:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_init_conversion(t_fractname *conv)
{
	conv[0].val = mandelbrot;
	conv[0].fract_name = "mandelbrot";
	conv[1].val = julia;
	conv[1].fract_name = "julia";
	conv[2].val = tricorn;
	conv[2].fract_name = "tricorn";
	conv[3].val = burning_ship;
	conv[3].fract_name = "burning ship";
	conv[4].val = heart;
	conv[4].fract_name = "heart";
	conv[5].val = celtic;
	conv[5].fract_name = "celtic";
	conv[6].val = quasi;
	conv[6].fract_name = "quasi";
	conv[7].val = star;
	conv[7].fract_name = "star";
	conv[8].val = newton;
	conv[8].fract_name = "newton";
	conv[9].val = custom;
	conv[9].fract_name = "custom";
}

int			ft_check_fract_name(t_frct *data, char *argv)
{
	int			i;
	char		*tmp;
	int			to_comp;
	t_fractname conversion[FRCT_NBR];

	i = -1;
	to_comp = -1;
	tmp = ft_strtolower(argv);
	ft_init_conversion(&conversion[0]);
	while (++i < FRCT_NBR)
		if (ft_strstr(conversion[i].fract_name, tmp))
			to_comp = conversion[i].val;
	free(tmp);
	if (to_comp == -1)
	{
		write(2, "\x1B[31m""error: ""\033[0m""\033[1m"
		"wrong fractal name!\n""\033[0m", 44);
		return (0);
	}
	if (!data->fract_type)
		data->fract_type = to_comp;
	else
		data->fract_type2 = to_comp;
	return (1);
}
