/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_key_code.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:04:52 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:04:55 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static	int	ft_is_supported_key(t_frct *data, int key_code)
{
	int i;

	i = 0;
	while (i < KEYS_NBR)
		if (data->keys_arr[i++] == key_code)
			return (1);
	return (0);
}

static void	ft_keys_wasdr(t_frct *data, int key_code)
{
	double	delta;

	delta = data->zoom > 0 ? 0.09 / data->zoom : 0.05;
	if (key_code == KEY_W || key_code == KEY_A
	|| key_code == KEY_S || key_code == KEY_D)
	{
		data->move_x += key_code == KEY_D ? delta : 0.0;
		data->move_x += key_code == KEY_A ? -delta : 0.0;
		data->move_y += key_code == KEY_W ? delta : 0.0;
		data->move_y += key_code == KEY_S ? -delta : 0.0;
	}
	else if (key_code == KEY_R)
		ft_init_fractal_params(data);
}

static void	ft_keys_zxqe(t_frct *data, int key_code)
{
	t_color	c;

	c.color = data->default_color;
	if (key_code == KEY_Z || key_code == KEY_X)
	{
		data->zoom *= key_code == KEY_Z ? 1.1 : 0.9;
		if (data->zoom >= 0 && data->zoom <= 0.2)
			data->zoom = 0.2;
	}
	else if ((data->mode && ft_strnequ(data->mode, "psychedelic", 11)) &&
		(key_code == KEY_Q || key_code == KEY_E))
	{
		c.rgb[red] += key_code == KEY_E ? 10 : -10;
		c.rgb[green] += key_code == KEY_E ? 5 : -5;
		c.rgb[blue] += key_code == KEY_E ? 42 : -42;
		if (c.color)
			data->default_color = c.color;
	}
}

static void	ft_keys_arr(t_frct *data, int key_code)
{
	if (key_code == KEY_LAR || key_code == KEY_RAR)
	{
		data->iter_nb += key_code == KEY_RAR ? 5 : -5;
		if (data->iter_nb <= 10)
			data->iter_nb = 10;
	}
}

int			ft_check_key_code(int k_n, t_frct *data)
{
	if (!ft_is_supported_key(data, k_n))
		return (1);
	if (k_n == KEY_ESC)
		exit(0);
	if (k_n == KEY_M)
		data->menu = data->menu == 1 ? 0 : 1;
	if (k_n == KEY_P)
		data->pause = data->pause == 0 ? 1 : 0;
	else if (k_n == KEY_Z || k_n == KEY_X || k_n == KEY_E || k_n == KEY_Q)
		ft_keys_zxqe(data, k_n);
	else if (k_n == KEY_W || k_n == KEY_A
	|| k_n == KEY_S || k_n == KEY_D || k_n == KEY_R)
		ft_keys_wasdr(data, k_n);
	else if (k_n == KEY_LAR || k_n == KEY_RAR)
		ft_keys_arr(data, k_n);
	mlx_clear_window(data->mlx_ptr, data->win_ptr);
	ft_draw_image(data);
	return (1);
}
