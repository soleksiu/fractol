/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_help.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:13 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:05:15 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#define DESCRIPTION "This project is meant to create "
#define DESCRIPTION2 "graphically beautiful fractals.\n"
#define USAGE "usage : fractol [fractal_name] [-c color_name -d window_width "
#define USAGE2 "window_height -i iterations_number -m color_mode] [-h] [-a]\n"
#define USAGE3 " Available fractals : Mandelbrot, Julia, Tricorn, "
#define USAGE4 "Burning ship, Heart, Celtic, Quasi, Star, Newton, Custom"
#define FLAG_D "   -d, --dimensions		set custom dimensions of the window\n"
#define FLAG_C "   -c, --color 			set default color for fractals. "
#define FLAG_I "   -i, --iterations 		set iterations number(max 4242)\n"
#define FLAG_M "   -m, --mode 			set color mode. "
#define FLAG_M2 "Available modes:\n	 			normal(default, work with rgb),"
#define M2 " monocrome, psychedelic(all colors)\n"
#define FLAG_C2 "Available colors:\n"
#define CLRS "	 			white, black, red, blue, yellow, cyan, magenta, "
#define CLRS2 "green, purple, pink\n"
#define HELP "   -h, --help 			display this help and exit\n"
#define INFO "   -a, --about 			display info about project"

void	ft_display_help(t_frct *data)
{
	write(1, USAGE, ft_strlen(USAGE));
	write(1, USAGE2, ft_strlen(USAGE2));
	write(1, USAGE3, ft_strlen(USAGE3));
	write(1, USAGE4, ft_strlen(USAGE4));
	write(1, "\n", 1);
	write(1, DESCRIPTION, ft_strlen(DESCRIPTION));
	write(1, DESCRIPTION2, ft_strlen(DESCRIPTION2));
	write(1, "\n", 1);
	write(1, FLAG_I, ft_strlen(FLAG_I));
	write(1, FLAG_D, ft_strlen(FLAG_D));
	write(1, FLAG_M, ft_strlen(FLAG_M));
	write(1, FLAG_M2, ft_strlen(FLAG_M2));
	write(1, M2, ft_strlen(M2));
	write(1, FLAG_C, ft_strlen(FLAG_C));
	write(1, FLAG_C2, ft_strlen(FLAG_C2));
	write(1, CLRS, ft_strlen(CLRS));
	write(1, CLRS2, ft_strlen(CLRS2));
	write(1, HELP, ft_strlen(HELP));
	write(1, INFO, ft_strlen(INFO));
	ft_error("", data);
}
