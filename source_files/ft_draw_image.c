/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_image.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:34 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:05:36 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_init_thred_params(t_frct *data, t_thread *th_data, int c)
{
	int	dy;

	dy = data->window_height / THREADS_NBR;
	th_data->begin_x = 0;
	th_data->begin_y = dy * c;
	if (dy * THREADS_NBR != data->window_height && c == THREADS_NBR - 1)
		dy = dy + (data->window_height - dy * THREADS_NBR);
	th_data->dx = data->window_width;
	th_data->dy = dy;
	th_data->iter_nb = data->iter_nb;
	th_data->color = data->default_color;
	th_data->window_width = data->window_width;
	th_data->window_height = data->window_height;
	th_data->zoom = data->zoom;
	th_data->move_x = data->move_x;
	th_data->move_y = data->move_y;
	th_data->c_re = data->c_re;
	th_data->c_im = data->c_im;
	th_data->img_bpp = data->img_bpp;
	th_data->img_size_line = data->img_size_line;
	th_data->img_endian = data->img_endian;
	th_data->img_addr = data->img_addr;
	th_data->mouse_motion = data->mouse_motion;
	th_data->flag_m = data->mode;
	th_data->flag_color = data->flag_color;
}

static void	ft_init_ft_pointers(void *(*fractal_ft_ptr[]) (void *data))
{
	fractal_ft_ptr[1] = ft_fract_mandelbrot;
	fractal_ft_ptr[2] = ft_fract_julia;
	fractal_ft_ptr[3] = ft_fract_tricorn;
	fractal_ft_ptr[4] = ft_fract_burn_ship;
	fractal_ft_ptr[5] = ft_fract_heart;
	fractal_ft_ptr[6] = ft_fract_celtic;
	fractal_ft_ptr[7] = ft_fract_quasi;
	fractal_ft_ptr[8] = ft_fract_star;
	fractal_ft_ptr[9] = ft_fract_newton;
	fractal_ft_ptr[10] = ft_fract_custom;
}

static void	ft_calculate_with_threads(t_frct *data)
{
	int				thread_c;
	void			*thread_return;
	pthread_t		threads[THREADS_NBR];
	t_thread		th_data[THREADS_NBR];
	void			*(*fractal_ft_ptr[FRCT_NBR + 1]) (void *data);

	ft_init_ft_pointers(fractal_ft_ptr);
	thread_c = 0;
	while (thread_c < THREADS_NBR)
	{
		ft_init_thred_params(data, &th_data[thread_c], thread_c);
		if (pthread_create(&threads[thread_c], NULL,
		fractal_ft_ptr[data->fract_type], (void *)(&th_data[thread_c])) == -1)
			ft_error("Error with creating thread", data);
		thread_c++;
	}
	thread_c = -1;
	while (++thread_c < THREADS_NBR)
		if (pthread_join(threads[thread_c], &thread_return) == -1)
			ft_error("Error with joining thread", data);
}

void		ft_draw_image(t_frct *data)
{
	unsigned int	text_color;

	text_color = ft_find_fonts_color(data, "corners_text");
	ft_calculate_with_threads(data);
	mlx_put_image_to_window(data->mlx_ptr, data->win_ptr, data->img_ptr, 0, 0);
	mlx_string_put(data->mlx_ptr, data->win_ptr, data->window_width - 225,
	data->window_height - 25, text_color, "press 'ESC' for exit");
	if (data->fract_type == julia)
		mlx_string_put(data->mlx_ptr, data->win_ptr, data->window_width
		/ 2 - 100, data->window_height - 25,
		0x00ffff00, "press 'P' to freeze image");
	mlx_string_put(data->mlx_ptr, data->win_ptr, data->window_width - 335,
		2, text_color, "press 'm' to show controls menu");
	mlx_string_put(data->mlx_ptr, data->win_ptr, 20, data->window_height - 25,
		text_color, "press 'r' to undo all actions");
	data->menu == 1 ? ft_print_controls_menu(data) : 0;
}
