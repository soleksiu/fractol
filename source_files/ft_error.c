/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:50 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:05:52 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "libft.h"
#define USAGE "usage : fractol [fractal_name] [-c color_name -d window_width "
#define USAGE2 "window_height -i iterations_number -m color_mode] [-h] [-a] \n"
#define USAGE3 " Available fractals : \n --> Mandelbrot \n --> Julia\n "
#define USAGE4 "--> Tricorn \n --> Burning ship \n --> Heart  \n --> Celtic \n "
#define USAGE5 "--> Quasi \n --> Star \n --> Newton \n --> Custom \n"

static void	ft_print_usage(void)
{
	write(2, USAGE, ft_strlen(USAGE));
	write(2, USAGE2, ft_strlen(USAGE2));
	write(2, USAGE3, ft_strlen(USAGE3));
	write(2, USAGE4, ft_strlen(USAGE4));
	write(2, USAGE5, ft_strlen(USAGE5));
}

void		ft_error(char *str, t_frct *data)
{
	if (ft_strequ(str, "usage"))
		ft_print_usage();
	else
	{
		write(2, str, ft_strlen(str));
		write(2, "\n", 1);
	}
	if (data->img_ptr)
		mlx_destroy_window(data->mlx_ptr, data->img_ptr);
	exit(-1);
}
