/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_fonts_color.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 12:33:10 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/12 12:33:11 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

unsigned int	ft_find_fonts_color(t_frct *data, char *type)
{
	t_color c;
	t_color to_check;

	if (!data->mode || !ft_strnequ(data->mode, "psychedelic", 11))
		return (0x00bdbdbd);
	to_check.color = data->default_color;
	if (to_check.rgb[red] > 125 || to_check.rgb[green] > 125
		|| to_check.rgb[blue] > 125)
	{
		if (ft_strequ(type, "menu"))
			c.color = 0x00003300;
		else if (ft_strequ(type, "corners_text"))
			c.color = 0x0000ffff;
	}
	else
	{
		if (ft_strequ(type, "menu"))
			c.color = 0x00bdbdbd;
		else if (ft_strequ(type, "corners_text"))
			c.color = 0x0000cdcd;
	}
	return (c.color);
}
