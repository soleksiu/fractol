/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags_manager.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:06:02 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:06:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define DIMENSIONS_ERR "please enter correct dimensions. "
#define DIMENSIONS_ERR2 "use -h or --help flag for help."
#define ITER_ERR "please enter correct iterations number. "
#define ITER_ERR2 "use -h or --help flag for help."
#define COLOR_ERR "please enter correct color. use -h "
#define COLOR_ERR2 "or --help flag for help."
#define INFO_T "This is a learning project of 42 "
#define INFO_T2 "school(obviously the best place to code in the Galaxy).\n"
#define INFO_T3 "Made by @soleksiu in Unit Factory. Kyiv. Ukraine. 2018."

static int	ft_check_flags_comb(t_frct *data, int argc, char **argv)
{
	if (!data->fract_type && (data->flag_a || data->flag_h) &&
	!(data->flag_a && data->flag_h))
		return (1);
	if (!data->fract_type
	|| (data->flag_c && data->flag_c + 1 == argc)
	|| (data->flag_d && data->flag_d + 1 == argc)
	|| (data->flag_d && data->flag_d + 2 == argc)
	|| (data->flag_h && data->flag_a)
	|| (data->flag_h && (data->flag_d || data->flag_c))
	|| (data->flag_a && (data->flag_d || data->flag_c)))
		return (0);
	if (data->flag_i)
	{
		data->iter_nb = ft_atoi(argv[data->flag_i]);
		if ((data->iter_nb < 10 || data->window_width > 4242))
			ft_error(ft_strjoin(ITER_ERR, ITER_ERR2), data);
	}
	return (1);
}

static int	ft_check_flag_name(t_frct *data, char **argv, int *i)
{
	if (!data->flag_c && (ft_strnequ(argv[*i], "-c", 3) ||
		ft_strnequ(argv[*i], "--color", 9)))
		data->flag_c = (*i)++;
	else if (!data->flag_d && (ft_strnequ(argv[*i], "-d", 2) ||
		ft_strnequ(argv[*i], "--dimensions", 14)) && ((*i) += 2))
		data->flag_d = *i - 2;
	else if (!data->flag_m && (ft_strnequ(argv[*i], "-m", 2) ||
		ft_strnequ(argv[*i], "--mode", 7)))
		data->flag_m = (*i)++;
	else if (!data->flag_a && (ft_strnequ(argv[*i], "-a", 3) ||
		ft_strnequ(argv[*i], "--about", 8)))
		data->flag_a = *i;
	else if (!data->flag_i && (ft_strnequ(argv[*i], "-i", 3) ||
		ft_strnequ(argv[*i], "--iterations", 13)) && ((*i) += 1))
		data->flag_i = *i;
	else if (ft_strnequ(argv[*i], "-h", 3) || ft_strnequ(argv[*i], "--help", 8))
		data->flag_h = *i;
	else if (!data->fract_type || !data->fract_type2)
	{
		if (!ft_check_fract_name(data, argv[*i]))
			return (0);
	}
	else
		return (0);
	return (1);
}

static int	ft_args_parsing(t_frct *data, int argc, char **argv)
{
	int i;

	i = 0;
	while (++i < argc)
	{
		if (!ft_check_flag_name(data, argv, &i))
			return (0);
	}
	if (data->flag_m)
	{
		if (ft_strnequ(argv[data->flag_m + 1], "monochrome", 10))
			data->mode = argv[data->flag_m + 1];
		else if (ft_strnequ(argv[data->flag_m + 1], "psychedelic", 11) ||
			ft_strnequ(argv[data->flag_m + 1], "normal", 7))
			data->mode = argv[data->flag_m + 1];
		else if (!data->mode)
			ft_error("error: wrong mode!", data);
	}
	return (ft_check_flags_comb(data, argc, argv));
}

static void	ft_define_color(t_frct *data, char **argv)
{
	if (ft_strnequ(argv[data->flag_c + 1], "white", 7))
		data->flag_color = 0x000ffffff;
	else if (ft_strnequ(argv[data->flag_c + 1], "red", 5))
		data->flag_color = 0x00ff0000;
	else if (ft_strnequ(argv[data->flag_c + 1], "blue", 6))
		data->flag_color = 0x000000ff;
	else if (ft_strnequ(argv[data->flag_c + 1], "yellow", 7))
		data->flag_color = 0x00ffff00;
	else if (ft_strnequ(argv[data->flag_c + 1], "cyan", 6))
		data->flag_color = 0x0000ffff;
	else if (ft_strnequ(argv[data->flag_c + 1], "magenta", 9))
		data->flag_color = 0x00ff00ff;
	else if (ft_strnequ(argv[data->flag_c + 1], "black", 7))
		data->flag_color = 0x00000001;
	else if (ft_strnequ(argv[data->flag_c + 1], "green", 7))
		data->flag_color = 0x0000ff00;
	else if (ft_strnequ(argv[data->flag_c + 1], "purple", 8))
		data->flag_color = 0x00800080;
	else if (ft_strnequ(argv[data->flag_c + 1], "pink", 6))
		data->flag_color = 0x00eea9b8;
	else
		ft_error(ft_strjoin(COLOR_ERR, COLOR_ERR2), data);
}

void		ft_flags_manager(int argc, char **argv, t_frct *data)
{
	if (argc < 2 || !(ft_args_parsing(data, argc, argv)))
		ft_error("usage", data);
	if (argc == 2 && data->fract_type)
		return ;
	if (argc > 2 || data->flag_a || data->flag_h)
	{
		if (data->flag_a)
		{
			write(1, ft_strjoin(INFO_T, INFO_T2), 89);
			write(1, INFO_T3, ft_strlen(INFO_T3));
			ft_error("", data);
		}
		if (data->flag_h)
			ft_display_help(data);
		if (data->flag_c)
			ft_define_color(data, argv);
		if (data->flag_d)
		{
			data->window_width = ft_atoi(argv[data->flag_d + 1]);
			data->window_height = ft_atoi(argv[data->flag_d + 2]);
			if ((data->window_width <= 0 || data->window_width > 2560)
				|| (data->window_height <= 0 || data->window_height > 1440))
				ft_error(ft_strjoin(DIMENSIONS_ERR, DIMENSIONS_ERR2), data);
		}
	}
}
