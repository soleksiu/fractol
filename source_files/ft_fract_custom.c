/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fract_custom.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 12:49:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/16 13:02:19 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int		ft_it(t_thread *data, t_cmplx *oldz, t_cmplx *newz)
{
	const double	min = 1e-6;
	const double	max = 1e+6;
	int				i;
	t_cmplx			delta;

	i = 0;
	delta.re = newz->re;
	delta.im = newz->im;
	while (i < data->iter_nb && (pow(newz->re, 2) + pow(newz->im, 2) < max)
		&& (pow(delta.re, 2) + pow(delta.im, 2) > min))
	{
		oldz->re = newz->re;
		oldz->im = newz->im;
		newz->re = sin(oldz->re) * cosh(oldz->im);
		newz->im = cos(oldz->re) * sinh(oldz->im);
		delta.re = fabs(oldz->re - newz->re);
		delta.im = fabs(oldz->im - newz->im);
		i++;
	}
	return (i);
}

static void		ft_rc(t_thread *data, t_cmplx *newz, t_coord *c)
{
	c->i = 0;
	newz->re = ((c->x_b - data->window_width / 2) / data->zoom) +
	data->move_x * 650;
	newz->im = ((c->y_b - data->window_height / 2) / data->zoom) +
	data->move_y * 650;
}

static void		ft_set_start_params(t_thread *data, t_coord *c)
{
	c->x_b = data->begin_x;
	c->y_b = data->begin_y;
	c->x_e = data->window_width;
	c->y_e = c->y_b + data->dy;
	c->i = 0;
}

void			*ft_fract_custom(void *d)
{
	size_t			color;
	t_cmplx			oldz;
	t_cmplx			newz;
	t_coord			c;
	t_thread		*data;

	data = (t_thread *)d;
	ft_set_start_params(data, &c);
	while (c.y_b < c.y_e)
	{
		while (c.x_b < c.x_e)
		{
			ft_rc(data, &newz, &c);
			c.i = ft_it(data, &oldz, &newz);
			color = ft_get_asid_color(data->iter_nb, c.i, data->color);
			ft_put_pixel_img(data, c.x_b, c.y_b, color);
			(c.x_b)++;
		}
		c.x_b = 0;
		(c.y_b)++;
	}
	return (NULL);
}
