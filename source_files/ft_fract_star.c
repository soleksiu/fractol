/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fract_star.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 15:37:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/16 15:41:07 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_set_start_params(t_thread *data, t_coord *c)
{
	c->x_b = data->begin_x;
	c->y_b = data->begin_y;
	c->x_e = data->window_width;
	c->y_e = c->y_b + data->dy;
	c->i = 0;
}

static void	ft_calccf(t_thread *data, t_cmplx *oldz, t_cmplx *newz, t_coord *c)
{
	data->c_re = 1.5 * (c->x_b - data->window_width / 2) /
	(0.5 * data->zoom * data->window_width) + data->move_x;
	data->c_im = (c->y_b - data->window_height / 2) /
	(0.5 * data->zoom * data->window_height) + data->move_y;
	newz->re = 0;
	newz->im = 0;
	oldz->re = 0;
	oldz->im = 0;
	c->i = 0;
}

static void	ft_iterate(t_thread *data, t_cmplx *oldz, t_cmplx *newz, t_coord *c)
{
	double zrsqr;
	double zisqr;

	while (c->i < data->iter_nb)
	{
		oldz->re = newz->re;
		oldz->im = newz->im;
		zrsqr = oldz->re * oldz->re;
		zisqr = oldz->im * oldz->im;
		newz->re = zrsqr * zrsqr + zisqr * zisqr -
		(6.0 * zrsqr * zisqr) + data->c_re;
		newz->im = -4.0 * oldz->re * oldz->im * (zrsqr - zisqr) + data->c_im;
		if ((newz->re * newz->re + newz->im * newz->im) > 4)
			break ;
		(c->i)++;
	}
}

void		*ft_fract_star(void *d)
{
	size_t			color;
	t_cmplx			oldz;
	t_cmplx			newz;
	t_coord			c;
	t_thread		*data;

	data = (t_thread *)d;
	ft_set_start_params(data, &c);
	while (c.y_b < c.y_e)
	{
		while (c.x_b < c.x_e)
		{
			ft_calccf(data, &oldz, &newz, &c);
			ft_iterate(data, &oldz, &newz, &c);
			if (data->flag_m && ft_strequ(data->flag_m, "psychedelic"))
				color = ft_get_asid_color(data->iter_nb, c.i, data->color);
			else
				color = ft_get_color(data->iter_nb, c.i, data, newz);
			ft_put_pixel_img(data, c.x_b, c.y_b, color);
			(c.x_b)++;
		}
		c.x_b = 0;
		(c.y_b)++;
	}
	return (NULL);
}
