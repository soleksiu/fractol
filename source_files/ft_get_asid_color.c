/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_asid_color.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 22:29:51 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/16 22:29:56 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

unsigned int	ft_get_asid_color(int max_iter, int iter_nb, unsigned int def_c)
{
	unsigned int component_shift;
	unsigned int color;

	color = 0;
	if (iter_nb == max_iter)
		return (0x00000000);
	if (iter_nb)
		iter_nb = (iter_nb % 1000) * 1000;
	component_shift = iter_nb ? 255 - iter_nb % 255 : 2;
	if (iter_nb >= 0 && (iter_nb < (max_iter / 2)))
	{
		color |= (0 | component_shift);
		color |= (0 | component_shift) << 8;
		color |= (0 | component_shift) << 16;
		color = def_c ^ color;
	}
	else
	{
		color |= (0 | component_shift);
		color |= (0 | component_shift) << 8;
		color |= (0 | component_shift) << 16;
		color = def_c | color;
	}
	return (color);
}
