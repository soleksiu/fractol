/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_color.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:08:44 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:08:49 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void			ft_check_def_c(t_color *c, t_thread *data, double coef)
{
	if (data->color == 0x000000ff)
	{
		c->rgb[blue] = (unsigned char)(255.0 * coef);
		c->rgb[green] = (unsigned char)0;
		c->rgb[red] = (unsigned char)0;
		c->rgb[alpha] = (unsigned char)0;
	}
	else if (data->color == 0x00ff0000)
	{
		c->rgb[blue] = (unsigned char)0;
		c->rgb[green] = (unsigned char)0;
		c->rgb[red] = (unsigned char)(255.0 * coef);
		c->rgb[alpha] = (unsigned char)0;
	}
}

static void			ft_check_flag_c(t_thread *data)
{
	if (data->flag_color)
	{
		if (data->flag_color != 0x0000ff00 &&
			data->flag_color != 0x000000ff && data->flag_color != 0x00ff0000)
			data->color = 0x0000ff00;
	}
}

unsigned int		ft_get_color(int m_it, int it_nb, t_thread *data, t_cmplx z)
{
	double	coef;
	t_color	c;

	if (it_nb == 0)
		return (0);
	coef = it_nb + 1 - log(log(sqrt(z.re * z.re + z.im * z.im))) / log(2);
	coef /= m_it;
	ft_check_flag_c(data);
	if (data->flag_m && ft_strequ(data->flag_m, "monochrome"))
	{
		c.rgb[blue] = (unsigned char)(255.0 * coef);
		c.rgb[green] = (unsigned char)(255.0 * coef);
		c.rgb[red] = (unsigned char)(255.0 * coef);
		c.rgb[alpha] = (unsigned char)(255.0 * coef);
	}
	else if (data->color == 0x0000ff00)
	{
		c.rgb[blue] = (unsigned char)0;
		c.rgb[green] = (unsigned char)(255.0 * coef);
		c.rgb[red] = (unsigned char)0;
		c.rgb[alpha] = (unsigned char)0;
	}
	ft_check_def_c(&c, data, coef);
	return (c.color);
}
