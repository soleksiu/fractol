/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_fractal_params.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 17:45:10 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/15 17:45:28 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_pick_default_color(t_frct *data)
{
	if (data->fract_type == mandelbrot)
		data->default_color = data->flag_color ? data->flag_color : 0x2a944a;
	else if (data->fract_type == julia)
		data->default_color = data->flag_color ? data->flag_color : 0x0000ff;
	else if (data->fract_type == tricorn)
		data->default_color = data->flag_color ? data->flag_color : 0x00000000;
	else if (data->fract_type == burning_ship)
		data->default_color = data->flag_color ? data->flag_color : 0x0029154a;
	else if (data->fract_type == heart)
		data->default_color = data->flag_color ? data->flag_color : 0x00ff0000;
	else if (data->fract_type == celtic)
		data->default_color = data->flag_color ? data->flag_color : 0x462226;
	else if (data->fract_type == quasi)
		data->default_color = data->flag_color ? data->flag_color : 0x434586;
	else if (data->fract_type == star)
		data->default_color = data->flag_color ? data->flag_color : 0xd78f5a;
	else if (data->fract_type == newton)
		data->default_color = data->flag_color ? data->flag_color : 0xa85428;
	else if (data->fract_type == custom)
		data->default_color = data->flag_color ? data->flag_color : 0x462226;
	if (!data->flag_color && data->fract_type != custom && (!data->mode
		|| ft_strnequ(data->mode, "normal", 7)))
		data->default_color = (data->fract_type == burning_ship) ?
		0x00ff0000 : 0x0000ff00;
}

static void	ft_set_fract_par(t_frct *data)
{
	if (data->fract_type == julia)
	{
		data->iter_nb = 50;
		data->zoom = 0.9;
		data->move_x = 0;
		data->move_y = 0;
		data->c_re = -0.7767;
		data->c_im = 0.148094;
	}
	else if (data->fract_type == mandelbrot)
	{
		data->zoom = 0.85;
		data->move_x = -0.5;
		data->move_y = 0;
	}
}

static void	ft_set_fract_par2(t_frct *data)
{
	if (data->fract_type == burning_ship)
	{
		data->zoom = 19;
		data->move_x = -1.77081;
		data->move_y = 0.0382168;
	}
	else if (data->fract_type == celtic)
	{
		data->zoom = 0.645;
		data->move_x = -0.15;
		data->move_y = 0;
	}
	else if (data->fract_type == quasi)
	{
		data->zoom = 0.645;
		data->move_x = 0;
		data->move_y = 0;
	}
}

static void	ft_set_fract_par3(t_frct *data)
{
	if (data->fract_type == newton)
	{
		data->zoom = 350;
		data->iter_nb = 15;
		data->move_x = 0;
		data->move_y = 0;
	}
	else if (data->fract_type == tricorn)
	{
		data->zoom = 0.57;
		data->move_x = -0.09;
		data->move_y = 0;
	}
}

void		ft_init_fractal_params(t_frct *data)
{
	if (!data->flag_i)
		data->iter_nb = 42;
	ft_pick_default_color(data);
	ft_set_fract_par(data);
	ft_set_fract_par2(data);
	ft_set_fract_par3(data);
	if (data->fract_type == heart && (data->zoom = 1))
	{
		data->move_x = -0.6;
		data->move_y = 0;
	}
	else if (data->fract_type == star)
	{
		data->zoom = 15190;
		data->move_x = -1.20704938;
		data->move_y = 0;
	}
	else if (data->fract_type == custom)
	{
		data->zoom = 100;
		data->move_x = -0.15;
		data->move_y = 0;
		data->iter_nb = 150;
	}
}
