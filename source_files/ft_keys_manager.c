/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keys_manager.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:09:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:09:05 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int		ft_exit_mouse(void *param)
{
	param = 0;
	exit(0);
	return (0);
}

static int		ft_check_key_code_mouse(int k_n, int x, int y, void *d)
{
	double move_delta_x;
	double move_delta_y;
	t_frct *data;

	data = (t_frct *)d;
	x *= 1;
	y *= 1;
	if (k_n == 5 || k_n == 4)
	{
		data->zoom *= k_n == 5 ? 1.1 : 0.9;
		if (data->zoom >= 0 && data->zoom <= 0.1)
			data->zoom = 0.1;
		move_delta_x = 0.0003 * (data->window_width / 2 - x) / data->zoom;
		move_delta_y = 0.0003 * (data->window_height / 2 - y) / data->zoom;
		if (data->zoom != 0.1)
		{
			data->move_x += k_n == 4 ? move_delta_x : -move_delta_x;
			data->move_y += k_n == 4 ? -move_delta_y : move_delta_y;
		}
	}
	mlx_clear_window(data->mlx_ptr, data->win_ptr);
	ft_draw_image(data);
	return (1);
}

static int		ft_mouse_coord(int x, int y, void *d)
{
	double	coef_re;
	double	coef_im;
	t_frct	*data;

	data = (t_frct *)d;
	if (data->fract_type != julia || x > data->window_width
		|| y > data->window_height)
		return (1);
	if (data->fract_type == julia && data->pause == 0)
	{
		coef_re = (data->window_width / 2 - x) * 0.0009;
		coef_im = (data->window_height / 2 - y) * 0.0055;
		data->c_re = -0.7 + (coef_im + coef_re);
		data->c_im = 0.27015 * coef_im + coef_re;
	}
	data->mouse_motion = 1;
	mlx_clear_window(data->mlx_ptr, data->win_ptr);
	ft_draw_image(data);
	data->mouse_motion = 0;
	return (1);
}

void			ft_keys_manager(t_frct *data)
{
	mlx_hook(data->win_ptr, 6, 0, ft_mouse_coord, data);
	mlx_hook(data->win_ptr, 17, 1L << 17, ft_exit_mouse, (void *)0);
	mlx_hook(data->win_ptr, 2, 5, ft_check_key_code, data);
	mlx_mouse_hook(data->win_ptr, ft_check_key_code_mouse, data);
}
