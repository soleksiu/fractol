/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_controls_menu.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:09:36 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:09:40 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#define CON "CONTROLS MENU"

#define E "E . . . . shift color forward"
#define Q "Q . . . . shift color backward"
#define R_R "> . . . . increase iterations"
#define R_L "< . . . . decrease iterations"
#define W "UP  . . . move image top"
#define S "DOWN  . . move image down"
#define D "LEFT  . . move image right"
#define A "RIGHT . . move image left"
#define Z "Z . . . . increase scale"
#define X "X . . . . decrease scale"

void	ft_print_controls_menu(t_frct *data)
{
	unsigned int	color;

	color = ft_find_fonts_color(data, "menu");
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 2, color, CON);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 20, color, E);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 35, color, Q);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 50, color, R_R);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 65, color, R_L);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 80, color, W);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 95, color, S);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 110, color, D);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 125, color, A);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 140, color, Z);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 24, 155, color, X);
}
