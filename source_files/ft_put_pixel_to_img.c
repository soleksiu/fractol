/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_pixel_to_img.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 14:34:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/10 14:34:02 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_put_pixel_img(t_thread *data, int x, int y, unsigned int color)
{
	y = (data->window_height - 1 - y) * data->img_size_line;
	x = x * ((data->img_size_line / data->window_width));
	data->img_addr[y + x] = (char)(color);
	data->img_addr[y + x + 1] = (char)(color >> 8);
	data->img_addr[y + x + 2] = (char)(color >> 16);
}
