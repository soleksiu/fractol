/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:10:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:10:14 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_init_starting_params(t_frct *data)
{
	ft_init_fractal_params(data);
	data->window_width = data->window_width == 0 ?
	M_WIDTH : data->window_width * 1;
	data->window_height = data->window_height == 0
	? M_HEIGHT : data->window_height * 1;
}

static void	ft_init_keys(t_frct *data)
{
	data->keys_arr[0] = KEY_E;
	data->keys_arr[1] = KEY_Q;
	data->keys_arr[2] = KEY_RAR;
	data->keys_arr[3] = KEY_LAR;
	data->keys_arr[4] = KEY_W;
	data->keys_arr[5] = KEY_S;
	data->keys_arr[6] = KEY_D;
	data->keys_arr[7] = KEY_A;
	data->keys_arr[8] = KEY_Z;
	data->keys_arr[9] = KEY_X;
	data->keys_arr[10] = KEY_ESC;
	data->keys_arr[11] = KEY_M;
	data->keys_arr[12] = KEY_R;
	data->keys_arr[13] = KEY_P;
}

static void	ft_init_mlx(t_frct *data)
{
	data->mlx_ptr = mlx_init();
	data->win_ptr = mlx_new_window(data->mlx_ptr, data->window_width,
	data->window_height, "soleksiu_fractol");
	if (!data->img_ptr)
	{
		data->img_ptr = mlx_new_image(data->mlx_ptr,
		data->window_width, data->window_height);
		data->img_addr = mlx_get_data_addr(data->img_ptr,
		&data->img_bpp, &data->img_size_line, &data->img_endian);
	}
}

int			main(int argc, char **argv)
{
	int		keys[KEYS_NBR];
	t_frct	fractol;
	t_frct	*data;

	data = &fractol;
	ft_memset((void *)data, 0, sizeof(fractol));
	data->keys_arr = &keys[0];
	ft_flags_manager(argc, argv, data);
	if (data->fract_type2 && fork())
		data->fract_type = data->fract_type2;
	ft_init_starting_params(data);
	ft_init_mlx(data);
	ft_init_keys(data);
	ft_keys_manager(data);
	ft_draw_image(data);
	mlx_loop(data->mlx_ptr);
	return (0);
}
